# M123 Repository

[TOC]

## Fileshare

## Auftrag
Wir müssen auf einen Ubuntu VM Samba installieren und nachher noch fileshare einrichten, sodass man einen Ordner vom Ubuntu Vm auf den WIndows 10 oder 22 VM aufmachen kann.

## Samba installieren

1. Ich habe einen Update mit dem Command Sudo apt update gemacht
![DNS BILD](./Images/Screenshot%202024-01-16%20111811.png_1Ubuntu.png)

2. Ich habe Samba mit dem Command sudo apt install Samba installiert
![fileshare Bild](./Images/Screenshot%202024-01-16%20111913.png)

3. Mit dem Command systemctl status smbd --no-pager -l, habe ich geschaut ob alles active lauft.
![fileshare Bild](./Images/Screenshot%202024-01-22%20212132.png)

4. Ich habe das system enabled
![fileshare](./Images/Screenshot%202024-01-22%20104731.png_smbd.png)

5. Mit dem Command sudo ufw allow samba, habe Samba erlaubt durch die Firewall zugehen.

6. Ich habe mein USER und Password gesetzt
![fileshare Bild](./Images/Screenshot%202024-01-22%20105618.png)

7. Fileshare einrichten

8. Ich habe ein Ordner erstellt.
!![fileshare Bild](./Images/Screenshot%202024-01-22%20213447.png_my%20test%20samba.png)

9. Ich habe den Ordner veröffentlicht
![fileshare Bild](./Images/_local%20network%20share.png)

10. Für den Windows CLient benutze ich den PC-02 vom Sota Auftrag, ich habe die IP-Adresse und namen beim add Network Location eingegeben und es ist gegangen.![filshare Bild](./Images/Screenshot%202024-01-23%20084203.png_sota.png)







