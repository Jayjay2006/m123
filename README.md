
[TOC]

# M123 Portfolio

## DHCP
-[Verzeichns](./DHCP/README.md)

## DNS
-[Verzeichnis](./DNS/README.md)

## Filesshare
-[Verzeichnis](./Fileshare/README.md)

## Druckerdienste
-[Verzeichnis](./Druckerdienste/README.md)

## Images