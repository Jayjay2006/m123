# M123 Repository

[TOC]

## Augabe 1

## DNS Server aufsetzen

# Auftrag
Wir müssen ein DNS Server aufsetzen mit Linux oder Windows(ich habe Linux Ubuntu genommen). EIn CLient noch mach und ein Privates Netzwerk.

# Was ist DNS

DNS steht für "Domain Name System". Es ist ein grundlegendes System im Internet, das dazu dient, menschenfreundliche Domainnamen in numerische IP-Adressen umzuwandeln. Diese IP-Adressen sind notwendig, damit Computer und Netzwerkgeräte effektiv miteinander kommunizieren können. DNS ermöglicht es, Websites und andere Ressourcen im Internet über leicht zu merkende Namen wie www.example.com zu identifizieren, anstatt sich die zugehörigen numerischen IP-Adressen merken zu müssen.

## 1. Vm Instalieren und prvates Netzwerk hinzufügen

Zuerst habe ich eine neu VM gemacht und das private Lab Netzwerk M_117 genommen von den Sota PC und wir nehmen beim aufsetzen des DNS Server wieder den Ubuntu Server Software, weil wie schon erwähnt werde ich diesen Server mit Linux Ubuntu aufsetzen.
![DHCP Bild](./images/Screenshot%202023-12-19%20094043.png)

## 2. VM aufsetzen

Ich habe meine Sprache und Tastatur Layout eingegeben
![DNS BIld](./images/Screenshot%202024-01-08%20093618.png_DNS%20SPrachen%20aufsetzen.png)


Als nextes habe ich mein Server Name, Benutzernamen und Passwort festgelegt
![DNS Bild](./images/Screenshot%202024-01-08%20093931.png_DNS_Namen_eingeben.png) 

## 3. Server Konfiguration

Als erstes habe ich mit disen Command einen Update gemacht
![DNS bILd](./images/Screenshot%202024-01-09%20092026.png_%20Get%20update%20command.png)

Nachher habe ich einen Upgrade gemacht und im unten stehenden Bild sehen sie wie dieser lauft,

![DNS bild](./images/Screenshot%202024-01-09%20092412.png_Get%20upgrade%20command.png)
Der Upgrade ging eindeutlich länger als den Update, am ANfang sagte es das es 52 Minuten gehen würde aber es ging nur 20 Minuten

Ich habe Bind9 installiert

![DNS Bild](./images/Screenshot%202024-01-09%20094159.png_Paket%20Konfiguration.png)

Ich habe noch die IP festgelegt und habe 2 DNS servers falls einer ausfällt
![DNS Bild](./images/Screenshot%202024-01-09%20110306.png_Ip%20DNs.png)

Mit den Befehl sudo nano /etc/bind//namedconf/options kommt man in der Haupt Konfiguration ich habe dort 8.8.8.8 als Forwarder hinzugefügt
![DNS Bild](./images/Screenshot%202024-01-09%20113421.png)

Mit den Befehl sudo /etc/bind/named.conf.local konnte ich denn Forward Zone wir im unten stehenden Bild erstellen.
![DNS Bild](./images/Screenshot%202024-01-09%20153709.png_Zone.png)

Ich habe den nachher deb reverse zone gemacht wie sie sehen im unten stehenden Bild
![DNS Bild](./images/Screenshot%202024-01-16%20084330.png_reverse%20zone.png)

Und im untenstehenden Bild habe ich  DNS Google/8.8.8.8 gepingt

![DNS Bild](./images/Screenshot%202024-01-16%20085013.png_ping.png)

Hier probiere ich 20 Minuten mit dem goodle.dns aufzulösen.
Wahrscheinlich wird dies nicht über meinen lokalen DNS-Server erfolgen, sondern über Google DNS. Die Meldung zeigt an, dass die Antwort nicht vom lokalen DNS-Server stammt, sondern von einem anderen, nämlich unserem Forwarder (dns.google). Der lokale DNS-Server ist nicht für diese Antwort verantwortlich.
![DNS Bild](./images/Screenshot%202024-01-16%20103525.png_9.tb.png)

## Troubleshooting 
1. Ich hatte keine Probleme beim Laden von der VM wie letztes mal am ANfang aber am ende vom aufsetzen wenn ich es restartet habe dauerte es viel länger als normalerweise.

2. Ich habe die Deutsche Tastatur anstastt die schweizerische Tastatur dies war sehr müsam, weil sie nicht den gleichen Layout haben, darum habe ich es um geändert auf die schweizerische Tastatur

3. Ich habe chatgpt gefragt wieso es so lang lät und Chatgpt hat mir einige Antworten gegeben

 Im untenstehenden Bild sagt chatgpt es liegt an HArdware vielleicht,  aber wie schon beim DHCP Server erwähnt beim Hardware liegt es nicht.
![DNS Bild](./images/Screenshot%202024-01-16%20092529.png_-1.%20tb.png)

Beim nexten Bild steht es könnte an der Konfiguration liegen, aber bei mir ging die Konfiguration des DNS Servers ziemlich gut, ich hatte keine Probleme bie konfigurieren selber.

![DNS Bild](./images/Screenshot%202024-01-16%20093002.png_2.tb.png)

Nachher hat es mir auch gesagt das die Festplatte Leistung nicht gut ist aber ich habe genug PLatz noch auf meiner Festplatte.
![DNS Bild](./images/Screenshot%202024-01-16%20093301.png_3.tb.png)

Chatgpt hat mir auch gesagt das ich vielleicht nicht die neuste Software hätte, aber ich habe Updates und Upgrades gemacht oben und die VM hat die neuste Ubuntu Server Software

![DNS Bild](./images/Screenshot%202024-01-16%20093559.png_4.tb.png)
![DNS Bild](./images/Screenshot%202024-01-16%20093915.png_5.tb.png)

Nochmal ein Grund der Chatgpt mir gesagt hat war, das es vielleicht Antisoftware in meiner VM hat und dies könnte die Lad Geschwindgkeit beeinträchten. Ich habe selber keine Antisoftware bei meiner VM installiert.
![DNS Bild](./images/Screenshot%202024-01-16%20094151.png_6.tb.png)

Noch ein Grund könnte sein das ich zu viele Snapshots habe und die VM ist dadurch überfordert. Ich habe selber keineSnapshots bis jetzt gemacht, weil es mir nie im SInn kam, dies ist sehr riskant falls etwas schlimmes passiert, aber es ist nichts schlims passiert.
![DNS Bild](./images/Screenshot%202024-01-16%20100452.png_7.tb.png)
 Im untenstehenden Bild sehen sie meine Anzahl Snapshots
 ![DNS Bild](./images/Screenshot%202024-01-16%20101305.png_8.tb.png)




