# M123 Repository

[TOC]

# Auftrag 1

# Aufgabe 1

## DHCP in a Nut Shell
Ich habe Laptop 1 und 2 als DHCP configuriert und habe auch den DHCP Server aktiviert. Nachdem ich denn Server aktiviert habe habe ich den DHCP Server noch eine IP und MAC-Adresse hinzugefügt. Nachdem ich dies gemacht habe, habe ich den Datenaustausch angeschaut. 


![**Topologie**](./images/Image_01.png)

Ein Photo noch zum Datenaustausch
![**Datenaustausch**](./images/MicrosoftTeams-image.png)

# Aufgabe 2

## Router Konfiguieren und auslesen
Ich bin zuerst im Ciscopackettracing beim Router im Cli gegangen und habe den User mode enabled damit ich Kommand ausführen kann. Der erste Kommand, welche ich probierte war R1#show ip dhcp pool dieser Befehl ist selbst erklärend, mit diesen Befehl sehe ich den DCHP pool.
Mit diese Befehle werde ich die nachfolgende aufgelistete Fragen beantworten

# Befehle:
 #show ip dhcp pool, #show ip dhcp binding und #show running-config

 # Fragen
1. Für welches Subnetz ist der DHCP Server aktiv?
2. Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort . als Tabelle)
3. In welchem Range vergibt der DHCP-Server IPv4 Adressen?
4. Was hat die Konfiguration ip dhcp excluded-address zur Folge?
5. Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

# Antowrten
1. 255.255.255.0
2. | IP ADRESSEN   | MAC-ADRESSEN    |
   |---------------|-----------------|
   | 192.168.27.29 | 0050.0F4F.1D82  |
   | 192.168.27.30 | 0001.632C.3508  |
   | 192.168.27.31 |  0009.7CA6.4CE3 |
   | 192.168.27.32 |  0007.ECB6.4534 |
   | 192.168.27.33 | 00E0.8F4E.65AA  |
   | 192.168.27.34 | 00D0.BC52.B29B  |
3. 192.168.27.1 - 192.168.27.254
4. Es sind 2 i ADRESSEN excluded es ist die IP 192.168.27.28 und 192.168.27.245
5. 54


Nachdem ich die Fragen alle benantwortet habe, bin ich zur nexte Aufgabe gegangen.

# Aufgabe2.2

## DORA - DHCP Lease beobachten
Ich habe den PC-02 gebracht zu einen DHCP Request zu machen, nachher habe ich die die IPv4 auf Static gewechselt und einen Simulation gestartet.

![**Simulation Photo**](./images/Screenshot%202023-12-05%20093708.png)

Nachdem Simulation habe ich die IPv4 wieder auf DHCP umgestellt. Nachdem ich es umgstellt habe ist ein gelbes PDU gekommen, ich es angeklicht und analysiert.

![**PDU Photo**](./images/Screenshot%202023-12-05%20101437.png)
Welcher OP-Code hat der DHCP Offer? 0x0000000000000002

Welcher OP-Code hat der DHCP Request?
0x0000000000000001

Welcher OP-Code hat der DHCP Acknowledge?
0x0000000000000002

An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?



# Auftrag 2

# DHCP Server Aufsetzen
Die Zielsetzung besteht darin, die Einrichtung eines Ubuntu-Servers vorzunehmen und anschließend eine DHCP-Anwendung über die Befehlszeile (Terminal) zu installieren. Die DHCP-Anwendung soll den DORA-Prozess erfolgreich mit einem Client durchführen können.
Die allgemeinen Schritte sind in etwa in dieser Dokumentation skizziert.

# DHCP
Ein DHCP-Dienst wird verwendet, um die automatisierte und vereinfachte Zuteilung von IP-Adressen zu ermöglichen. Bei einer Benutzeranzahl von 10 ist der Einsatz eines DHCP-Servers sinnvoll und wird empfohlen. Die Schritte der Verbindung und Kommunikation zwischen DHCP-Server und Client sind im DORA-Prozess beschrieben. Dabei erfolgt zunächst das genaue Suchen der einzelnen Netzwerkkomponenten per Broadcast. Der DHCP-Server erhält dann über Port 67 eine Anfrage zur Vergabe und Reservierung einer IP-Adresse und sendet die relevanten Daten (Lease-Time, IP, Gateway, DNS) zur Verbindung an den Client zurück (via Broadcast). Der Client akzeptiert die Offerte und meldet dies per Broadcast dem DHCP-Server. Schließlich erhält der DHCP-Server die akzeptierte Offerte zurück und beginnt mit der Reservierung der IP-Adresse, möglicherweise auch mit der direkten Vergabe der IP-Adresse.

## Logik
Im untenstehenden bild sehen sie wie der logischer Plan des DHCP Auftrags ist.

![**Logischer Plan**](./images/Screenshot%202023-12-18%20092952.png)

## Netzwerkadapter

1.	Mit «Add Network» ein neues Netzwerkadapter hinzufügen.

2. Für M117_LAB sollten Sie unbedingt sicherstellen, dass die Optionen "Connect a host virtual adapter to this network" und "Use local DHCP service to distribute IP address to VMs" deaktiviert sind. Dies verhindert, dass der DHCP von VMware in unser Netzwerk eingreift. Geben Sie außerdem die Subnetz-IP-Adresse ein.
![DHCP Bild](./images/Screenshot%202023-12-19%20094043.png)

3. Nochmal ein Netzwerkadapter hinzufügen und bei den Optionen NAT wählen "Connect a host virtual adapter to this network" und "Use local DHCP service to distribute IP address to VMs"

## Instalation Ubuntu Server

1. Nachdem ich die Software bei der VM Instalation rein getan habe und ich die VM erstellt habe kam das untenstehende Bild. Da muss man Try or install Ubuntu Server anklicken.

![VM start Bild](./images/Screenshot%202023-12-18%20094158.png)

2. Ich habe nach meine beliebige Einstellungen durch geklickt.

3. Die Namen kann man selber entscheiden
![VM Bild](./images/Screenshot%202023-12-18%20100424.png)

4. Nach einige Minuten erscheint das Terminal, man kann die Befehle nachher da ein tippen.

## Installation des Servers via Terminal

Sämtliche Befehle werden unter Verwendung des Root-Status ausgeführt. Der Root-User entspricht dem Administrator unter Windows und verfügt über sämtliche Ausführungs- und Bearbeitungsrechte. Durch die Verwendung von "sudo su" kann in den Root-Modus gewechselt werden. Es ist jedoch nicht zwingend erforderlich, und stattdessen kann weiterhin "sudo" vor jedem Befehl eingegeben werden.

1. Mit sudo apt install isc-dhcp-server, laden wir den DHCP-Dienst herunter.

2. Nach einer weile sollte es herunter geladen sein, aber damit es funktioniert müssen wir es noch konfigurieren. Damit wir wissen das er laüft geben wir den Befehl: sudo systemctl status isc-dhcp-server ein.
![DHCP Bild Befehl](./images/Screenshot%202023-12-18%20104101.png)

3. Als nextes habe ich ein Backup gemacht, dies habe ich mit dem Befehl: 
sudo cp /etc/dhcp/dhcpd.conf  /etc/dhcp/dhcpd.conf.backup gemacht.

4. Nachher habe ich die range bestummen und der Gateway adress und der DNS server Adress.

5. Ich habe nachher noch mein DHCP-server restartet mit dem Command: systemctl restart isc-dhcp-server

6. Nachher habe ich den Status mit dem Command: systemctl status isc-dhcp-server überprüft

## DHCP-Client konfigurieren

Ich probiert den CLient zu konfigurieren, damit er eine Dynamisch IP-Adresse erhält die machte ich mit de folgenden Command

nano /etc/network/interfaces

auto eth0
iface eth0 inet dhcp

7. Ich habe den DHCP Server mit dem Command: systemctl restarts network-manager restartet.

8. Und der letzte schritt war den DHCP-Server Adresse zu überprüfen mit dem Command:ifconfig.

## Troubleshooting

Es will nicht bei mir laden und wenn es ladet frezzed es konstant. Es liegt nicht am Internet, weil es in der Tbz und bei mir zuhause gleich war. Auch wenn es ladet und ich Commands eingebe ladewn die entweder nicht oder es stürrzt direkt nachher ab. Ich hatte den DHCP Server fertig, aber löschte es, weil später solche probleme kamen und fing es von neu a, aber ich hatte beim zweiten DHCP Server genau die gleiche Probleme.

Mein Passwort ist nicht gegangen ich konnte kein Hashtag typen, jedes mal wo ich Hastag typen wollte kam die Nummer 3 und ich habe auch verschidene Tastatur Kombinationen  gemacht, aber es ging nicht und damit musste ich nochmal den DHCP Server neu machen, aber ich hatte immer noch die selbe Probleme.

Die VM konnte ich selber meistens auch nicht zu machen, ich musste dies durch Task Manager machen.

Nachdem ich rechechiert habe fan dich heruas das es an meine VM Hardwareeinstellung legen kann.

So sehen meine Hardware Einstellungen aus 

![DHCP Bild](./images/Screenshot%202023-12-19%20092121.png)









Ich habe meine Processors von 2 auf 4 getan und habe den Memory(RAM) auf 4.9 Gb hoch getan

![DHCP Bild](./images/Screenshot%202023-12-19%20092642.png)




Später habe ich weiter rechechiert und fand raus das es vielleicht an meine Hardware Komponenten legen kann(beim Laptop) aber es stellte sich fest das ich genug gut Komponenten hatte.(Ich habe eine Intel i7 12th Gen, 16 GB Ram, über 500 GB storage und GPU NVIDIA GEforce MX5)




GPU
![DHCP Bild](./images/Screenshot%202023-12-19%20093244.png)



CPU
![DHCP Bild](./images/Screenshot%202023-12-19%20093316.png)



